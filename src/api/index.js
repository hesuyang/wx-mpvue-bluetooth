var Fly = require('flyio/dist/npm/wx')
var fly = new Fly // 创建实例

// 添加拦截器
fly.interceptors.request.use((config, promise) => {
  // config.headers
  console.log('config', config)
  return config
})

// 响应拦截
fly.interceptors.response.use(res => {
  console.log('fly res', res.data)
  return res
})

// 配置请求基准地址
// fly.config.baseURL = 'https://xxx.com/api'
fly.config.baseURL = 'https://www.easy-mock.com/mock/5cb5988e5b8cc913a423de05/web'

// 测试flyio
// https://www.easy-mock.com/mock/5cb5988e5b8cc913a423de05/web/test_flyio

// 测试flyio
export const getFlyio = () => fly.get('/test_flyio')

export default fly
